import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Person } from '../person';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleService } from '../people.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html', 
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class PersonDetailsComponent implements OnInit {
  professions: string[] = ['jedi', 'bounty hunter', 'princess', 'sith lord'];
  @Input() person: Person;
  sub: any;

  constructor(private peopleService: PeopleService,
              private route: ActivatedRoute,
              private router: Router) { }
  
  ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
      let id = Number.parseInt(params['id']);
      this.person = this.peopleService.get(id);
    });
  }

  ngOnDestroy(): void{
    this.sub.unsubscribe();
  }

  gotoPeoplesList(){
    window.history.back();
  }
  savePersonDetails(){
    alert(`saved!!! ${JSON.stringify(this.person)}`);
  }
}
