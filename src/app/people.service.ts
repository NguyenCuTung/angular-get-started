import { Injectable } from '@angular/core';
import { Person } from './person';

const PEOPLE: Person[] = [
  { id: 1, name: 'Luke Skywalker', height: 177, weight: 70, profession: '' },
  { id: 2, name: 'TungNC', height: 172, weight: 64, profession: '' },
  { id: 3, name: 'TuyetDT', height: 160, weight: 49, profession: '' }
];

@Injectable()
export class PeopleService {

  getAll(): Person[] {
    return PEOPLE.map(p => this.clone(p));
  }
  get(id: number): Person {
    return this.clone(PEOPLE.find(p => p.id === id));
  }
  save(person: Person) {
    let originalPerson = PEOPLE.find(p => p.id === person.id);
    if (originalPerson) Object.assign(originalPerson, person);
    // saved moahahaha
  }

  private clone(object: any) {
    // hack
    return JSON.parse(JSON.stringify(object));
  }
}